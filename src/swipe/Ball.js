import React, { Component } from 'react'
import { View, Text, StyleSheet, Animated } from 'react-native'

class Ball extends Component {
    UNSAFE_componentWillMount() {
        this.position = new Animated.ValueXY(0, 0); // 2D value class
        Animated.spring(this.position, { // spring => to modify
            toValue: { x: 200, y: 500 }
        }).start()
    }
    render() {
        console.log(this.position) // {"x":0, "y":0}
        return (
            <Animated.View style={this.position.getLayout()}>
                <View style={styles.ball} />
            </Animated.View>
        )
    }
}

export default Ball

const styles = StyleSheet.create({
    ball: {
        height: 60,
        width: 60,
        borderRadius: 30,
        borderWidth: 30,
        borderColor: 'black'
    }
})