import { transform } from '@babel/core';
import React, { Component } from 'react'
import { Animated, PanResponder, View, Dimensions } from 'react-native'

const SCREEN_WIDTH = Dimensions.get('window').width; // 17
const SWIPE_THRESHOLD = 0.25 * SCREEN_WIDTH; // 20
const SWIPE_OUT_DURATION = 250; // 24

export class Deck extends Component {
    static defaultProps = { // 31
        onSwipeRight: () => { }, // 31.1 when Deck component is called, function will check if the props properties are not exist, Js will read the defaultProps
        onSwipeLeft: () => { }// 31.2
    }

    constructor(props) {
        super(props) // wajib
        const position = new Animated.ValueXY(); // 6
        const panResponder = PanResponder.create({
            onStartShouldSetPanResponder: (evt, gestureState) => true, // 1 saat 'press down ' screen; dokumentasi -> initialisasi = TRUE
            onPanResponderMove: (evt, gestureState) => {  // 2 - work setelah state.panResponder.panHandlers
                // debugger; // debugger paused tuk bantu console di chrome -> Scoupe/local/gestureState // harus dalam mode debugger untuk display value
                // play dan paused video -> ngerefer kepada console 
                /**
                 * dX dY = move finger in single gesture // MAIN CONCERNED
                 * vx vy = seberapa cepat gerakan moving // MAIN CONCERNED
                 * xo yo mirip dgn moveX moveY ?? berdasar pergeseran gerakan finger
                 * dx: 3.495147705078125
                    dy: 0
                    moveX: 170.76283264160156
                    moveY: 139.8766632080078
                    numberActiveTouches: 1 // jumlah klik 
                    stateID: 0.4557115520015378
                    vx: 1.6108759948988617e-8
                    vy: 0
                    x0: 167.26768493652344 //
                    y0: 139.8766632080078
                    _accountsForMovesUpTo: 216971866
                 */

                position.setValue({ x: gestureState.dx, y: gestureState.dy }) // 7

            }, // saat geser screen 
            onPanResponderRelease: (evt, gesture) => { // 3 saat udh ga disentuh ; released all touches
                if (gesture.dx > SWIPE_THRESHOLD) { // 21
                    // console.log('right')
                    this.forceSwipe('right');// 22
                } else if (gesture.dx < -SWIPE_THRESHOLD) { // 21.1
                    console.log('left')
                    this.forceSwipe('left');// 25
                } else {
                    this.resetPosition()// 19
                }
            }
        });
        this.state = { // 4
            panResponder,
            position, // 8
            index: 0 // 29
        }
    }

    forceSwipe(direction) { // 23
        const x = direction === 'right' ? SCREEN_WIDTH : -SCREEN_WIDTH; // 26 itenary expression
        Animated.timing(this.state.position, {
            toValue: { x, y: 0 }, // 26.1
            duration: SWIPE_OUT_DURATION,
        }).start(
            () => this.onSwipeComplete(direction) // 27
        );
    }

    onSwipeComplete(direction) { // 28
        const { onSwipeLeft, onSwipeRight, data } = this.props;
        const item = data[this.state.index]; // 30
        direction === 'right' ? onSwipeRight(item) : onSwipeLeft(item); // 30.1 
        this.setState({ index: this.state.index + 1 }) // 32
        this.state.position.setValue({ x: 0, y: 0 })
    }

    resetPosition() { // 20
        Animated.spring(this.state.position, {
            toValue: { x: 0, y: 0 }
        }).start();
    }

    getCardStyle() { // 13
        const { position } = this.state; // 15
        const rotate = position.x.interpolate({
            // inputRange: [-500, 0, 500], // 16 --> 500 width in px ??
            inputRange: [-SCREEN_WIDTH * 1.5, 0, SCREEN_WIDTH * 1.5], // 18
            outputRange: ['-120deg', '0deg', '120deg'] // 16.1
        })
        return {
            ...position.getLayout(),
            // transform: [{ rotate: '-45deg' }] // 14 manual rotate:'-/+ ... deg'
            transform: [{ rotate }] // 17
        }
    }

    renderCards() {
        return this.props.data.map((item, index) => {
            if (index === 0) { // 10
                return (
                    <Animated.View
                        key={item.id} // 13
                        // style={this.state.position.getLayout()} // 9 // 11
                        style={this.getCardStyle()} // 14
                        {...this.state.panResponder.panHandlers} // 5 // 12 panHandlers -> default agar ada respon saat 'press' -> respone ke evt && gestureState
                    >
                        {this.props.renderCard(item)}
                    </Animated.View>
                )
            }
            return this.props.renderCard(item)
        })
    }

    render() {
        console.log(this.state.position)
        return (
            // <Animated.View
            //     style={this.state.position.getLayout()} // 9
            //     {...this.state.panResponder.panHandlers} // 5 
            // >
            <View // 13 
            >
                {this.renderCards()}
            </View>
            // </Animated.View>
        )
    }
}

export default Deck
