import React from 'react'
import { Text, View } from 'react-native'
import _01Swipe from './src/_01Swipe'

const App = () => {
  return (
    <View style={{flex:1}}>
      <_01Swipe />
    </View>

  )
}

export default App
