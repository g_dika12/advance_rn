import React, { Component } from 'react'
import { StyleSheet, Text, View, Image } from 'react-native'
import { Button, Card } from 'react-native-elements';
// import { Card } from 'react-native-elements/dist/card/Card';
import Deck from './swipe/Deck'

const DATA = [
    { id: 1, text: 'Card #1', uri: 'https://images.unsplash.com/photo-1453060590797-2d5f419b54cb?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&auto=format&fit=crop&w=1050&q=80' },
    { id: 2, text: 'Card #2', uri: 'https://images.unsplash.com/photo-1508612227635-e7cbb9bdf54d?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1020&q=80' },
    { id: 3, text: 'Card #3', uri: 'https://images.unsplash.com/photo-1532983330958-4b32bbe9bb0e?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&auto=format&fit=crop&w=1050&q=80' },
    { id: 4, text: 'Card #4', uri: 'https://images.unsplash.com/photo-1584535553837-33e69fc4ca4d?ixid=MXwxMjA3fDB8MHxzZWFyY2h8MTB8fGZyZWV8ZW58MHwwfDB8&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60' },
    { id: 5, text: 'Card #5', uri: 'https://images.unsplash.com/photo-1504194104404-433180773017?ixid=MXwxMjA3fDB8MHxzZWFyY2h8OXx8ZnJlZXxlbnwwfDB8MHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60' },
    { id: 6, text: 'Card #6', uri: 'https://images.unsplash.com/photo-1495539406979-bf61750d38ad?ixid=MXwxMjA3fDB8MHxzZWFyY2h8MTN8fGZyZWV8ZW58MHwwfDB8&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60' },
    { id: 7, text: 'Card #7', uri: 'https://images.unsplash.com/photo-1483884105135-c06ea81a7a80?ixid=MXwxMjA3fDB8MHxzZWFyY2h8MTJ8fGZyZWV8ZW58MHwwfDB8&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60' },
    { id: 8, text: 'Card #8', uri: 'https://images.unsplash.com/photo-1469881317953-097ae79770ea?ixid=MXwxMjA3fDB8MHxzZWFyY2h8MTV8fGZyZWV8ZW58MHwwfDB8&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60' },
];

class _01Swipe extends Component {

    renderCard(item) {
        return (
                <Card
                    key = {item.id}
                >
                    <Card.Image source= {{ uri: item.uri }} />
                    <Card.Title>{item.text}</Card.Title>
                    <Text>Customizing Cards</Text>
                    <Button
                        icon={{name:'code'}}
                        backgroundColor={'#03A9F4'}
                        title="View Now"
                    />
                </Card>
        )
    }

    render() {
        return (
            <View style={styles.container}>
                <Deck
                    data={DATA}
                    renderCard={this.renderCard}
                />
            </View>
        )
    }
}

export default _01Swipe;

const styles = StyleSheet.create({
    container: {
        flex: 1,
    }
})